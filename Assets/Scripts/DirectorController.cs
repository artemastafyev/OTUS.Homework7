using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DirectorController : MonoBehaviour
{
    [SerializeField] List<Character> attackers;
    [SerializeField] float attackTime = 3;
    [SerializeField] float startTime = 2;

    void Awake()
    {
        StartCoroutine(RunCutscene());
    }

    public IEnumerator RunCutscene()
    {
        yield return new WaitForSeconds(startTime);

        foreach (var attacker in attackers)
        {
            attacker.AttackEnemy();

            yield return new WaitForSeconds(attackTime);
        }

        yield return null;
    }
}
